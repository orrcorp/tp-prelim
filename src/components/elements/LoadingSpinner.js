import React, { Component } from 'react';
import spinner from  '../../images/spinner.png';

export class LoadingSpinner extends Component {
  render() {
    return (
        <div className="row loading-row">  
            <div className="col-sm-12"><img src={spinner} className="loading-spinner" alt="loading"/></div>
            <div className="col-sm-12"><div className="loading-text"><h4>Loading</h4></div></div>
        </div>
    )
  }
}

export default LoadingSpinner
