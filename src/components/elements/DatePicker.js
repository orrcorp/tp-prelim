import React, { Component, Fragment } from 'react'

export class DatePicker extends Component {
    render() {
        return (
        <Fragment>
            <div className="filter-container">
                <h3>FILTER RANGE:</h3>
                <form className="row">                    
                    <div className="form-group col"><input type="text" id="startdate" className="form-control" placeholder="start"/></div>    
                    <div className="form-group col"><input type="text" id="enddate" className="form-control" placeholder="end" /></div>
                </form>
            </div>
        </Fragment>
        )
    }
}

export default DatePicker
