import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { SFCardContent, NRCardContent, WorkOrderCardRowContent, PartsCardRowContent, SupportCardRowContent } from './CardRowContent';

export class AccountCardRow extends Component {
    render() {
        return (
            <Fragment key={ this.props.col_3 } >
                <div className="col-sm-5">{ this.props.col_1 }</div>
                <div className="col-sm-3">{ this.props.col_2 }</div>
                <div className="col-sm-4">{ this.props.col_3 }</div>
            </Fragment>
        )
    }
}

export class LocationCardRow extends Component {
    render() {
        return (
            <Fragment> {/* List the locations in the row, with expandable container to display all class/parts, etc. */}
                <form>
                    <div className="row row-card"> 
                        <div className="col-sm-2">
                            <div className="form-group">
                                <label htmlFor="part-no"> Class</label>
                                <input id="part-no" name="text" type="text" className="form-control-plaintext" defaultValue={this.props.col_1} />
                            </div>  
                        </div>
                        <div className="col-sm-3">
                            <div className="form-group">
                                <label htmlFor="part-no"> Part</label>
                                <input id="part-no" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_2} />
                            </div>               
                        </div>
                        <div className="col-sm-2">
                            <div className="form-group">
                                <label htmlFor="part-desc">MFR</label>
                                <input id="part-desc" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_3} />
                            </div>      
                        </div>
                        <div className="col-sm-3">
                            <div className="form-group">
                                <label htmlFor="ordered">Model</label>
                                <input id="ordered" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_4} />
                            </div>  
                        </div>
                        <div className="col-sm-2">
                            <div className="form-group">
                                <label htmlFor="received">On Site</label>
                                <input id="received" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_5} />
                            </div>  
                        </div>
                    </div>
                </form>
            </Fragment>
        )
    }
}

export class WorkOrderCardRow extends Component {
    state = {            
        id: this.props.id,
        opened: this.props.opened
    }
    toggled = () => { this.setState({ opened: !this.state.opened, }) 
        console.log("WO Clicked") 
    }
    render() {
        return (
            <Fragment key={ this.props.id } >
                <form>
                    <div className="row row-card" style={{padding: "1.25rem"}}> 
                        <div className="col-sm-2">
                            <div className="form-group">
                                <label htmlFor="line-number" style={{color:"#00C389"}}>W-Ord # </label><br/>
                                <button id="line-number" type="button" className="btn btn-orr-row-butt" onClick={this.toggled}>{this.props.col_1}</button>
                            </div>       
                        </div>
                        <div className="col-sm-2">
                            <div className="form-group">
                                <label htmlFor="part-no"> Location</label>
                                <input id="part-no" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_3} />
                            </div>               
                        </div>
                        <div className="col-sm-2">
                            <div className="form-group">
                                <label htmlFor="part-desc">Inst Prod</label>
                                <input id="part-desc" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_6} />
                            </div>      
                        </div>
                        <div className="col-sm-2">
                            <div className="form-group">
                                <label htmlFor="ordered">Date </label>
                                <input id="ordered" name="text" type="text" className="form-control-plaintext" defaultValue={this.props.col_7} />
                            </div>  
                        </div>
                        <div className="col-sm-2">
                            <div className="form-group">
                                <label htmlFor="received">Time</label>
                                <input id="received" name="text" type="text" className="form-control-plaintext" defaultValue={this.props.col_8} />
                            </div>  
                        </div>
                        <div className="col-sm-1">
                            <div className="form-group">
                                <label htmlFor="mt">MT</label>
                                <div className="form-check">
                                    <input name="checkbox" id="mt" type="checkbox" className="form-check-input" value="" checked={this.props.col_9} />
                                </div>
                            </div>
                        </div> 
                        <div className="col-sm-1">
                            <div className="form-group">
                                <label htmlFor="md">MD</label>
                                <div className="form-check">
                                    <input name="checkbox" id="md" type="checkbox" className="form-check-input" value="" checked={this.props.col_10} />
                                </div>
                            </div>
                        </div>                        
                    </div>
                    <div className={'card-expand' + (this.state.opened ? ' animate' :'') }>
                        <WorkOrderCardRowContent 
                            col_1 = { this.props.col_1 }
                            col_2 = { this.props.col_2 }
                            col_3 = { this.props.col_3 }
                            col_4 = { this.props.col_4 }
                            col_5 = { this.props.col_5 }
                            col_6 = { this.props.col_6 }
                            col_7 = { this.props.col_7 }
                            col_8 = { this.props.col_8 }
                            col_9 = { this.props.col_9 }
                            col_10 = { this.props.col_10 }
                            col_11 = { this.props.col_11 }
                            col_12 = { this.props.col_12 }
                            col_13 = { this.props.col_13 }
                            col_14 = { this.props.col_14 }
                            col_15 = { this.props.col_15 }
                            col_16 = { this.props.col_16 }
                            col_17 = { this.props.col_17 }
                            col_18 = { this.props.col_18 }
                            col_19 = { this.props.col_19 }
                            col_20 = { this.props.col_20 }
                            col_21 = { this.props.col_21 }
                            col_22 = { this.props.col_22 }
                            col_23 = { this.props.col_23 }
                            col_24 = { this.props.col_24 }
                            col_25 = { this.props.col_25 }
                            col_26 = { this.props.col_26 }
                            col_27 = { this.props.col_27 }
                            col_28 = { this.props.col_28 }
                            col_29 = { this.props.col_29 }
                            col_30 = { this.props.col_30 }
                        />                    
                    </div>
                </form>            
            </Fragment>
        )
    }
}

export class PartsCardRow extends Component {
    state = {            
        id: this.props.id,
        opened: this.props.opened
    }
    toggled = () => { this.setState({ opened: !this.state.opened, }) 
        console.log("Line # Clicked") 
    }
    render() {
        return (
            <Fragment key={ this.props.id } >
                <form>
                    <div className="row row-card" style={{padding: "1.25rem"}}> 
                        <div className="col-sm-3">
                            <div className="form-group">
                                <label htmlFor="line-number" style={{color:"#00C389"}}><b>LINE #</b></label><br/>
                                <button id="line-number" type="button" className="btn btn-orr-row-butt" onClick={this.toggled}>{this.props.col_2}</button>
                            </div>       
                        </div>
                        <div className="col-sm-2">
                            <div className="form-group">
                                <label htmlFor="part-no"> Part #</label>
                                <input id="part-no" name="text" type="text" className="form-control-plaintext" defaultValue={this.props.col_3} />
                            </div>               
                        </div>
                        <div className="col-sm-3">
                            <div className="form-group">
                                <label htmlFor="part-desc">Description</label>
                                <input id="part-desc" name="text" type="text" className="form-control-plaintext" defaultValue={this.props.col_4} />
                            </div>      
                        </div>
                        <div className="col-sm-1">
                            <div className="form-group">
                                <label htmlFor="ordered">Ord </label>
                                <input id="ordered" name="text" type="text" className="form-control-plaintext" defaultValue={this.props.col_5} />
                            </div>  
                        </div>
                        <div className="col-sm-1">
                            <div className="form-group">
                                <label htmlFor="received">Rec</label>
                                <input id="received" name="text" type="text" className="form-control-plaintext" defaultValue={this.props.col_6} />
                            </div>  
                        </div>
                        <div className="col-sm-2">
                            <div className="form-group">
                                <label htmlFor="status">Status</label>
                                <input id="status" name="text" type="text" className="form-control-plaintext" defaultValue={this.props.col_7} />
                            </div>
                        </div>
                    </div>
                    <div className={'card-expand' + (this.state.opened ? ' animate' :'') }>
                        <PartsCardRowContent 
                            col_2 = { this.props.col_2 }
                            col_8 = { this.props.col_8 }
                            col_9 = { this.props.col_9 }
                            col_10 = { this.props.col_10 }
                            col_11 = { this.props.col_11 }
                            col_12 = { this.props.col_12 }
                            col_13 = { this.props.col_13 }
                            col_14 = { this.props.col_14 }
                            col_15 = { this.props.col_15 }
                            col_16 = { this.props.col_16 }
                            col_17 = { this.props.col_17 }
                            col_18 = { this.props.col_18 }
                            col_19 = { this.props.col_19 }
                            col_20 = { this.props.col_20 }
                            col_21 = { this.props.col_21 }
                            col_22 = { this.props.col_22 }
                            col_23 = { this.props.col_23 }
                            col_24 = { this.props.col_24 }
                        />                    
                    </div>
                </form>            
            </Fragment>
        )
    }
}

export class FollowUpCardRow extends Component {
    render() {
        return (
            <Fragment>
                <form>
                    <div className="row row-card">
                        <div className="col-lg-2">
                            <div className="form-group">
                                <label htmlFor="workorder">WO# </label>
                                <input id="workorder" name="text" type="text" className="form-control-plaintext" defaultValue={this.props.col_1} />
                            </div>  
                        </div>
                        <div className="col-lg-4">
                            <div className="form-group">
                                <label htmlFor="location">LOCATION </label>
                                <input id="location" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_2} />
                            </div>  
                        </div>
                        <div className="col-lg-6">
                            <div className="form-group">
                                <label htmlFor="product">PRODUCT </label>
                                <input id="product" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_3} />
                            </div>  
                        </div>
                    </div>
                    <div className="row row-card">
                        <div className="col-lg-3">
                            <div className="form-group">
                                <label htmlFor="barcode">BARCODE </label>
                                <input id="barcode" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_4} />
                            </div>  
                        </div>
                        <div className="col-lg-3">
                            <div className="form-group">
                                <label htmlFor="condition">CONDITION </label>
                                <input id="condition" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_5} />
                            </div>  
                        </div>
                        <div className="col-lg-3">
                            <div className="form-group">
                                <label htmlFor="action">ACTION </label>
                                <input id="action" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_6} />
                            </div>  
                        </div>
                        <div className="col-lg-3">
                            <div className="form-group">
                                <label htmlFor="status">STATUS </label>
                                <input id="status" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_7} />
                            </div>  
                        </div>
                    </div>
                    <div className="row row-card">
                        <div className="col-lg-12">
                            <div className="form-group">
                                <label htmlFor="notes">NOTES </label>
                                <p id="notes">{this.props.col_8} </p>
                            </div>  
                        </div>
                    </div>
                </form>
            </Fragment>
        )
    }
}

export class DashCardRow extends Component {
    render() {
        return (
            <Fragment key={ this.props.id } >
                <div className={`col-lg-${this.props.field3}`}>
                    <div className="card dash-card">                                    
                        <div className="card-body">
                            <div className="row">
                                <div className="col-lg-8">                      
                                    <div className="card-header dash-card-header"><h5>{this.props.field1}:</h5></div>
                                </div>
                                <div className="col-lg-4">
                                    <h1 className="display-5 dash-card-totals">{this.props.field4}</h1>
                                </div>     
                            </div>
                        </div>
                        <div className="card-footer bg-transparent">
                            <Link to={`/${this.props.field5}`} className="btn btn-lg btn-block btn-outline-secondary">{this.props.field6}</Link>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export class SupportCardRow extends Component {
    state = { opened: this.props.opened }
    toggled = () => { this.setState({ opened: !this.state.opened, }) 
        console.log("Support Item Clicked") 
    }
    render() {
        return (
            <Fragment key={ this.props.id } >
                <form>
                    <div className="row row-card" >
                        <button type="button" class="btn btn-lg btn-block orr-btn-block" onClick={this.toggled}>
                            <h3>{this.props.col_1}<i class="fas fa-arrow-circle-down fa-lg" style={{float: 'right', marginTop: '5px'}}></i></h3>
                        </button>
                    </div>
                    <div className={'card-expand' + (this.state.opened ? ' animate' :'') }>
                        <SupportCardRowContent 
                            opened = { this.props.opened }
                            col_1 = { this.props.col_1 }
                            col_2 = { this.props.col_2 }
                            col_3 = { this.props.col_3 }
                        />
                    </div>
                </form>            
            </Fragment>
        )
    }
}

export class SFCardRow extends Component {
    render() {
        return (
            <Fragment>
                <SFCardContent
                    key = { this.props.id }
                    col_1 = { this.props.col_1 }
                    col_2 = { this.props.col_2 }
                    col_3 = { this.props.col_3 }
                    col_4 = { this.props.col_4 }
                    col_5 = { this.props.col_5 }
                    col_6 = { this.props.col_6 }
                    col_7 = { this.props.col_7 }
                    col_8 = { this.props.col_8 }
                    col_9 = { this.props.col_9 }
                    col_10 = { this.props.col_10 }
                    col_11 = { this.props.col_11 }
                />
            </Fragment>
        )
    }
}

export class NRCardRow extends Component {
    render() {
        return (
            <Fragment>
                <NRCardContent
                    key = { this.props.id }
                    col_1 = { this.props.col_1 }
                    col_2 = { this.props.col_2 }
                    col_3 = { this.props.col_3 }           
                />
            </Fragment>
        )
    }
}

export class TempCardRow extends Component {
    state = { opened: this.props.opened }
    toggled = () => { this.setState({ opened: !this.state.opened, }) 
        console.log("Line Number Clicked") 
    }
    render() {
        return (
            <Fragment key={ this.props.id } >
   
            </Fragment>
        )
    }
}
