import React, { Component, Fragment } from 'react';
import { AccountCardRow, LocationCardRow, WorkOrderCardRow, PartsCardRow, FollowUpCardRow, DashCardRow, SupportCardRow,TempCardRow  } from './CardRows';

export class DashCardContainers extends Component {
    render() {
        return (
            <Fragment>               
                <DashCardRow 
                    key = { this.props.id }
                    field1= { this.props.field1 }
                    field2 = { this.props.field2 }
                    field3 = { this.props.field3 }
                    field4 = { this.props.field4 }
                    field5 = { this.props.field5 }
                    field6 = { this.props.field6 }
                    field7 = { this.props.field7 }
                    field8 = { this.props.field8 }
                />
            </Fragment>
        )
    }
}

export class AccountCardContainers extends Component {
    render() {  
        return (
            <Fragment>               
                <div className="row">
                    <div className="col-md-12">
                        <div className="card data-card">
                            <div className="card-body">
                                <div className="row">                                                                
                                    <AccountCardRow
                                        key = { this.props.col_3 }
                                        col_1 = { this.props.col_1 }
                                        col_2 = { this.props.col_2 }
                                        col_3 = { this.props.col_3 }
                                        col_4 = { this.props.col_4 }
                                        col_5 = { this.props.col_5 }
                                        col_6 = { this.props.col_6 }                
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}
export class LocationCardContainers extends Component {
    render() {
        return (
            <Fragment>               
                <div className="row">
                    <div className="col-md-12">
                        <div className="card data-card">
                            <div className="card-body">                                
                                    <LocationCardRow 
                                        key = { this.props.id }
                                        col_1 = { this.props.col_1 }
                                        col_2 = { this.props.col_2 }
                                        col_3 = { this.props.col_3 }
                                        col_4 = { this.props.col_4 }
                                        col_5 = { this.props.col_5 }
                                        col_6 = { this.props.col_6 }     
                                    />
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export class WorkOrderCardContainers extends Component {
    render() {
        return (
            <Fragment>               
                <div className="row">
                    <div className="col-md-12">
                        <div className="card data-card">
                            <div className="card-body" style={{padding: "0px"}}> 
                                <WorkOrderCardRow 
                                    key = { this.props.id }
                                    col_1= { this.props.col_1 }
                                    col_2 = { this.props.col_2 }
                                    col_3 = { this.props.col_3 }
                                    col_4 = { this.props.col_4 }
                                    col_5 = { this.props.col_5 }
                                    col_6 = { this.props.col_6 }
                                    col_7 = { this.props.col_7 }
                                    col_8 = { this.props.col_8 }
                                    col_9 = { this.props.col_9 }
                                    col_10 = { this.props.col_10 }
                                    col_11 = { this.props.col_11 }
                                    col_12 = { this.props.col_12 }
                                    col_13 = { this.props.col_13 }
                                    col_14 = { this.props.col_14 }
                                    col_15 = { this.props.col_15 }
                                    col_16 = { this.props.col_16 }
                                    col_17 = { this.props.col_17 }
                                    col_18 = { this.props.col_18 }
                                    col_19 = { this.props.col_19 }
                                    col_20 = { this.props.col_20 }
                                    col_21 = { this.props.col_21 }
                                    col_22 = { this.props.col_22 }
                                    col_23 = { this.props.col_23 }
                                    col_24 = { this.props.col_24 }
                                    col_25 = { this.props.col_25 }
                                    col_26 = { this.props.col_26 }
                                    col_27 = { this.props.col_27 }
                                    col_28 = { this.props.col_28 }
                                    col_29 = { this.props.col_29 }
                                    col_30 = { this.props.col_30 }
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export class PartsCardContainers extends Component {
    render() {
        return (
            <Fragment>               
                <div className="row">
                    <div className="col-md-12">
                        <div className="card data-card">
                             <div className="card-body" style={{padding: "0px"}}>
                                <PartsCardRow 
                                    key = { this.props.id }
                                    opened = {this.props.opened}
                                    col_1= { this.props.col_1 }
                                    col_2 = { this.props.col_2 }
                                    col_3 = { this.props.col_3 }
                                    col_4 = { this.props.col_4 }
                                    col_5 = { this.props.col_5 }
                                    col_6 = { this.props.col_6 }
                                    col_7 = { this.props.col_7 }
                                    col_8 = { this.props.col_8 }
                                    col_9 = { this.props.col_9 }
                                    col_10 = { this.props.col_10 }
                                    col_11 = { this.props.col_11 }
                                    col_12 = { this.props.col_12 }
                                    col_13 = { this.props.col_13 }
                                    col_14 = { this.props.col_14 }
                                    col_15 = { this.props.col_15 }
                                    col_16 = { this.props.col_16 }
                                    col_17 = { this.props.col_17 }
                                    col_18 = { this.props.col_18 }
                                    col_19 = { this.props.col_19 }
                                    col_20 = { this.props.col_20 }
                                    col_21 = { this.props.col_21 }
                                    col_22 = { this.props.col_22 }
                                    col_23 = { this.props.col_23 }
                                    col_24 = { this.props.col_24 }
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export class FollowUpCardContainers extends Component {
    render() {
        return (
            <Fragment>               
                <div className="row">
                    <div className="col-md-12">
                        <div className="card data-card">
                            <div className="card-body">
                                    <FollowUpCardRow 
                                        key = { this.props.id }
                                        col_1= { this.props.col_1 }
                                        col_2 = { this.props.col_2 }
                                        col_3 = { this.props.col_3 }
                                        col_4 = { this.props.col_4 }
                                        col_5 = { this.props.col_5 }
                                        col_6 = { this.props.col_6 }
                                        col_7 = { this.props.col_7 }
                                        col_8 = { this.props.col_8 }
                                    />
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export class SupportCardContainers extends Component {
    render() {  
        return (
            <Fragment>               
                <div className="row">
                    <div className="col-md-12">
                        <div className="card data-card">
                            <div className="card-body" style={{padding: "0px"}}>
                                    <SupportCardRow
                                        key = { this.props.col_1 }
                                        opened = {this.props.opened}
                                        col_1 = { this.props.col_1 }
                                        col_2 = { this.props.col_2 }
                                        col_3 = { this.props.col_3 }           
                                    />                     
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export class TempCardContainers extends Component {
    render() {
        return (
            <Fragment>               
                <div className="row">
                    <div className="col-md-12">
                        <div className="card data-card">
                            <div className="card-body" style={{padding: "0px"}}>             
                            <TempCardRow />
                                {/* <TempCardRow 
                                    key = { this.props.id }
                                    col_1= { this.props.col_1 }
                                    col_2 = { this.props.col_2 }
                                    col_3 = { this.props.col_3 }
                                    col_4 = { this.props.col_4 }
                                    col_5 = { this.props.col_5 }
                                    col_6 = { this.props.col_6 }
                                    col_7 = { this.props.col_7 }
                                    col_8 = { this.props.col_8 }
                                /> */}
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}
