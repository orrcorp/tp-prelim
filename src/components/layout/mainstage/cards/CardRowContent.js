import React, { Component, Fragment } from 'react';
import { NavLink } from 'react-router-dom';

export class CardRowContent extends Component {
    render() {
        return (
            <Fragment>
                    <div className="card-content-expanded">
                        <div className="row">
                            <div className="account-header">
                                <div className="row">
                                    <div className="col-sm-3 card-inner-account-name">{this.props.col_8}</div>
                                    <div className="col-sm-4 card-inner-account-address trunc ">{this.props.col_9}</div>
                                    <div className="col-sm-5 card-inner-account-location ">{this.props.col_10}</div>
                                </div>
                            </div>                        
                        </div>
                        <div className="row card-inner-section-head">
                            <div className="col-md-3">ORD #</div>
                            <div className="col-md-3">ORD DATE</div>
                            <div className="col-md-2">EXP SHIP </div>
                            <div className="col-md-2">EXP REC </div>
                            <div className="col-md-2">REC'D</div>
                        </div>
                        <div className="row card-inner-section-content">
                            <div className="col-md-3">{this.props.col_11}</div>
                            <div className="col-md-3">{this.props.col_12}</div>
                            <div className="col-md-2">{this.props.col_13}</div>
                            <div className="col-md-2">{this.props.col_14}</div>
                            <div className="col-md-2">{this.props.col_15}</div>
                        </div>
                        <div className="row card-inner-section-head">                            
                            <div className="col-md-2"> TYPE</div>                            
                            <div className="col-md-2"> STATUS</div>
                            <div className="col-md-2">COURIER</div>
                            <div className="col-md-2">PRIORITY</div>
                            <div className="col-md-4">TRACK #</div>
                        </div>
                        <div className="row card-inner-section-content">                            
                            <div className="col-md-2">{this.props.col_16}</div>                            
                            <div className="col-md-2">{this.props.col_17}</div>
                            <div className="col-md-2">{this.props.col_18}</div>
                            <div className="col-md-2">{this.props.col_19}</div>
                            <div className="col-md-4">{this.props.col_20}</div>
                        </div>
                        <div className="row card-inner-section-head">
                            <div className="col-md-3">SAGE PO</div>
                            <div className="col-md-3">FROM LOC</div>
                            <div className="col-md-3">TO LOC</div>
                            <div className="col-md-3">NOTES</div>
                        </div>
                        <div className="row card-inner-section-content" style={{height: "100%"}}>
                        <div className="col-md-3">{this.props.col_21}</div>    
                            <div className="col-md-3">{this.props.col_22}</div>
                            <div className="col-md-3">{this.props.col_23}</div>
                            <div className="col-md-3">{this.props.col_24}</div>
                        </div>
                    </div>
            </Fragment>
        )
    }
}

export class WorkOrderCardRowContent extends Component {
    render() {
        const fullAddress = this.props.col_2 + ' ' + this.props.col_3 + this.props.col_4 + this.props.col_5;
        return (
            <Fragment>
                <div className="card-content-expanded">
                    <div className="row">
                        <div className="account-header" >
                            <div className="row">
                                <div className="col-sm-9">
                                    <div className="row"><div className="card-inner-account-name"> {this.props.col_11}</div></div>
                                    <div className="row"><div className="card-inner-account-address"> {this.props.col_12}</div></div>
                                    <div className="row"><div className="card-inner-account-location">{fullAddress}</div></div>
                                </div>
                                <div className="col-sm-3">
                                    <div className="row">
                                        <div className="card-inner-account-name">WO-{this.props.col_1}</div>
                                    </div>                   
                                    <div className="row">
                                        <div className="">
                                            <NavLink className="btn btn-outline-success" to="/parts" style={{marginTop: "13px"}}>Related Parts</NavLink>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><hr/>
                    <div className="row card-inner-section-content">
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label htmlFor="address"> ADDRESS</label>
                                <input id="address" name="text" type="text" className="form-control-plaintext trunc" defaultValue={fullAddress} />
                            </div>               
                        </div>
                        <div className="col-sm-3">
                            <div className="form-group">
                                <label htmlFor="contact-person"> CONTACT PERSON</label>
                                <input id="contact-person" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_14} />
                            </div>               
                        </div>
                        <div className="col-sm-3">
                            <div className="form-group">
                                <label htmlFor="contact"> CONTACT #</label>
                                <input id="contact" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_15} />
                            </div>               
                        </div>   
                    </div>
                    <div className="row card-inner-section-content">
                        <div className="col-sm-5">
                            <div className="form-group">
                                <label htmlFor="inst-prod"> INST PROD</label>
                                <input id="inst-prod" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_6} />
                            </div>               
                        </div>
                        <div className="col-sm-5">
                            <div className="form-group">
                                <label htmlFor="product"> PRODUCT</label>
                                <input id="product" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_16} />
                            </div>               
                        </div>
                        <div className="col-sm-2">
                            <div className="form-group">
                                <label htmlFor="order-type"> ORDER TYPE</label>
                                <input id="order-type" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_17} />
                            </div>               
                        </div>   
                    </div><hr/>
                    <div className="row card-inner-section-content">
                        <div className="col-sm-4">
                            <div className="form-group">
                                <label htmlFor="comp-class"> COMP CLASS</label>
                                <input id="comp-class" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_24} />
                            </div>               
                        </div>
                        <div className="col-sm-4">
                            <div className="form-group">
                                <label htmlFor="manufacturer"> MANUFACTURER</label>
                                <input id="manufacturer" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_26} />
                            </div>               
                        </div>
                        <div className="col-sm-4">
                            <div className="form-group">
                                <label htmlFor="model"> MODEL</label>
                                <input id="model" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_27} />
                            </div>               
                        </div>   
                    </div>
                    <div className="row card-inner-section-content">
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label htmlFor="type"> TYPE</label>
                                <input id="ctype" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_25} />
                            </div>               
                        </div>
                        <div className="col-sm-2">
                            <div className="form-group">
                                <label htmlFor="on-site"> ON SITE</label>
                                <input id="on-site" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_28} />
                            </div>               
                        </div>
                        <div className="col-sm-2">
                            <div className="form-group">
                                <label htmlFor="due"> DUE</label>
                                <input id="due" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_29} />
                            </div>               
                        </div>
                        <div className="col-sm-2">
                            <div className="form-group">
                                <label htmlFor="plus-twelve"> + 12</label>
                                <input id="plus-twelve" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_30} />
                            </div>               
                        </div>  
                    </div><hr/>
                    <div className="row card-inner-section-content">
                        <div className="col-sm-3">
                            <div className="form-group">
                                <label htmlFor="visit-type"> VISIT TYPE</label>
                                <input id="visit-type" name="text" type="text" className="form-control-plaintext trunc" defaultValue={fullAddress} />
                            </div>               
                        </div>
                        <div className="col-sm-3">
                            <div className="form-group">
                                <label htmlFor="po-num"> PO NUM</label>
                                <input id="po-num" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_14} />
                            </div>               
                        </div>
                        <div className="col-sm-3">
                            <div className="form-group">
                                <label htmlFor="work-ticket"> WORK TICKET #</label>
                                <input id="work-ticket" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_15} />
                            </div>               
                        </div>
                        <div className="col-sm-3">
                            <div className="form-group">
                                <label htmlFor="access-hours"> ACCESS HOURS</label>
                                <input id="access-hours" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_15} />
                            </div>               
                        </div>
                    </div>
                    <div className="row card-inner-section-content">
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label htmlFor="address"> PREVIOUS NOTES</label>
                                <input id="address" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_22} />
                            </div>               
                        </div>
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label htmlFor="contact-person"> LOCATION NOTES</label>
                                <input id="contact-person" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_23} />
                            </div>               
                        </div> 
                    </div>
                </div>
            </Fragment>
        )
    }
}

export class PartsCardRowContent extends Component {
    render() {
        return (
            <Fragment>
                    <div className="card-content-expanded">
                        <div className="row">
                            <div className="account-header" >
                                <div className="row">
                                    <div className="col-sm-9">
                                        <div className="row"><div className="card-inner-account-name"> {this.props.col_8}</div></div>
                                        <div className="row"><div className="card-inner-account-address"> {this.props.col_9}</div></div>
                                        <div className="row"><div className="card-inner-account-location">{this.props.col_10}</div></div>
                                    </div>
                                    <div className="col-sm-3">
                                        <div className="row">
                                            <div className="card-inner-account-name">LN-{this.props.col_2}</div>
                                        </div>                   
                                        <div className="row">
                                            <div className="">
                                                <NavLink className="btn btn-outline-success" to="/work-orders" style={{marginTop: "13px"}}>Related WO</NavLink>
                                            </div>
                                         </div>
                                    </div>
                                </div>
                            </div>
                        </div><hr/>
                        <div className="row card-inner-section-content">
                            <div className="col-sm-3">
                                <div className="form-group">
                                    <label htmlFor="order-no"> ORD #</label>
                                    <input id="order-no" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_11} />
                                </div>               
                            </div>
                            <div className="col-sm-3">
                                <div className="form-group">
                                    <label htmlFor="ord-date"> ORD DATE</label>
                                    <input id="ord-date" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_12} />
                                </div>               
                            </div>
                            <div className="col-sm-2">
                                <div className="form-group">
                                    <label htmlFor="exp-ship"> EXP SHIP</label>
                                    <input id="exp-ship" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_13} />
                                </div>               
                            </div>
                            <div className="col-sm-2">
                                <div className="form-group">
                                    <label htmlFor="exp-ship"> EXP SHIP</label>
                                    <input id="exp-ship" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_14} />
                                </div>
                            </div>
                            <div className="col-sm-2">
                                <div className="form-group">
                                    <label htmlFor="received"> REC'D</label>
                                    <input id="received" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_15} />
                                </div>
                            </div>
                        </div><hr/>
                        <div className="row card-inner-section-content">
                            <div className="col-sm-2">
                                <div className="form-group">
                                    <label htmlFor="type"> TYPE</label>
                                    <input id="type" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_16} />
                                </div>               
                            </div>
                            <div className="col-sm-2">
                                <div className="form-group">
                                    <label htmlFor="status"> STATUS</label>
                                    <input id="status" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_17} />
                                </div>               
                            </div>
                            <div className="col-sm-2">
                                <div className="form-group">
                                    <label htmlFor="courier"> COURIER</label>
                                    <input id="courier" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_18} />
                                </div>               
                            </div>
                            <div className="col-sm-2">
                                <div className="form-group">
                                    <label htmlFor="priority"> PRIORITY</label>
                                    <input id="priority" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_19} />
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="form-group">
                                    <label htmlFor="track-no"> TRACK #</label>
                                    <input id="track-no" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_20} />
                                </div>
                            </div>
                        </div><hr/>
                        <div className="row card-inner-section-content">
                            <div className="col-sm-3">
                                <div className="form-group">
                                    <label htmlFor="sage-po"> SAGE PO</label>
                                    <input id="sage-po" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_21} />
                                </div>               
                            </div>
                            <div className="col-sm-3">
                                <div className="form-group">
                                    <label htmlFor="from-loc"> FROM LOC</label>
                                    <input id="from-loc" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_22} />
                                </div>               
                            </div>
                            <div className="col-sm-3">
                                <div className="form-group">
                                    <label htmlFor="to-loc"> TO LOC</label>
                                    <input id="to-loc" name="text" type="text" className="form-control-plaintext trunc" defaultValue={this.props.col_23} />
                                </div>               
                            </div>
                            <div className="col-sm-3">
                                <div className="form-group">
                                    <label htmlFor="notes"> NOTES</label>
                                    <p className="">{this.props.col_24}</p>
                                </div>
                            </div>
                        </div>
                    </div>
            </Fragment>
        )
    }
}

export class SupportCardRowContent extends Component {
    render() {
        return (
            <Fragment>
                    <div className="card-content-expanded">
                        <div className="row card-inner-section-content">
                            <div className="col-md-12">                                 
                                <h5 style={{marginTop: '10px'}}>{ this.props.col_2 }</h5>
                            </div>
                        </div>
                    </div>
            </Fragment>
        )
    }
}

export class TempCardContent extends Component {
    render() {
        return (
            <Fragment>
                    <div className="card-content-expanded">
                        <div className="row">
                            <div className="account-header">
                                <div className="row">
                                    <div className="col-sm-2 card-inner-account-name"> ATT FL </div>
                                    <div className="col-sm-5 card-inner-account-address">FL1749 - 2901 Dixie Hwy Oakland Park</div>
                                </div>
                            </div>                        
                        </div>
                        <div className="row card-inner-section-head">
                            <div className="col-md-3">ORD #</div>
                            <div className="col-md-3">ORD DATE</div>
                            <div className="col-md-2">EXP SHIP </div>
                            <div className="col-md-2">EXP REC </div>
                            <div className="col-md-2">REC'D</div>
                        </div>
                        <div className="row card-inner-section-content">
                            <div className="col-md-3">00005469</div>
                            <div className="col-md-3">03/12/19</div>
                            <div className="col-md-2">03/13/19</div>
                            <div className="col-md-2">03/18/19</div>
                            <div className="col-md-2">03/18/19</div>
                        </div>
                        <div className="row card-inner-section-head">                            
                            <div className="col-md-2"> TYPE</div>                            
                            <div className="col-md-2"> STATUS</div>
                            <div className="col-md-2">COURIER</div>
                            <div className="col-md-2">PRIORITY</div>
                            <div className="col-md-4">TRACK #</div>
                        </div>
                        <div className="row card-inner-section-content">                            
                            <div className="col-md-2">Priority</div>                            
                            <div className="col-md-2">In Transit</div>
                            <div className="col-md-2">FEDEX</div>
                            <div className="col-md-2">2 Day Air</div>
                            <div className="col-md-4">ZG3897898646852826</div>
                        </div>
                        <div className="row card-inner-section-head">
                            <div className="col-md-3">SAGE PO</div>
                            <div className="col-md-3">FROM LOC</div>
                            <div className="col-md-3">TO LOC</div>
                            <div className="col-md-3">NOTES</div>
                        </div>
                        <div className="row card-inner-section-content" style={{height: "100%"}}>
                        <div className="col-md-3">123456789</div>    
                            <div className="col-md-3">Safety Parts Depot <br/>101 South US HWY 1 <br/> Ft Lauderdale, FL 33334 </div>
                            <div className="col-md-3">ORR Safety <br/>11601 Interchange Drive <br/> Louisville, KY  40229 </div>
                            <div className="col-md-3">(1) 87-120042-001 SVA, 
                            <br/>(1) 87-120043-001 System Cartridge,
                            <br/>(1) 87-120044-001 Test Cartridge, 
                            <br/>(1) 87-120058-001 EMT Connector Kit, 
                            <br/>(1) 87-120039-001 SPDT Microswitch Kit,
                            <br/>(1) 87-120039-501 SPDT Microswitch Kit</div>
                        </div>
                    </div>
            </Fragment>
        )
    }
}

export class SFCardContent extends Component {
    render() {
        return (
            <Fragment key={ this.props.id } >
                <div className="card">
                    <div className="card-header"><b>Work Order #: </b> { this.props.col_2 }</div>
                    <div className="card-body">                        
                        <p><b>Technician: </b> { this.props.col_3 }</p>
                        <p><b>Company: </b> { this.props.col_4 }</p>
                        <p><b>Location: </b> { this.props.col_5 }</p>
                        <p><b>Address: </b> { this.props.col_6 }</p>
                        <p><b>City State: </b> { this.props.col_7 }, { this.props.col_8 }  { this.props.col_9 }</p>
                        <p><b>Contact: </b> { this.props.col_10 }</p>
                        <p><b>Phone: </b> { this.props.col_11 }</p>                    
                    </div>
                    <div className="card-footer"><b>SF ID:</b> { this.props.col_1 }</div>
                </div>
            </Fragment>
        )
    }
}

export class NRCardContent extends Component {
    render() {
        return (
            <Fragment key={ this.props.id } >
                <div className="card">
                    <div className="card-header"><b>Location:</b> { this.props.col_1 }</div>
                    <div className="card-body"><p><b>Barcode ID: </b> { this.props.col_3 }</p></div>
                    <div className="card-footer"><b>Date Asseted:</b> { this.props.col_2 }</div>
                </div>
            </Fragment>
        )
    }
}
