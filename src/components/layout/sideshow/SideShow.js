import React, { Component, Fragment } from 'react';
import AccordionSection from '../sideshow/accordion/AccordionSection';
import DatePicker from '../../elements/DatePicker';
import BreadCrumbs from '../../elements/BreadCrumbs';
// import { NavLink, Link } from 'react-router-dom';
import { NavLink } from 'react-router-dom';

class SideShow extends Component {
    state = {
        sidecontent: [
            {
                id: 1,
                opened: false,
                parent: "AMGEN",
                account: "AMGEN - KY",
                location: "3223 Shelbyville Rd"        
            },
            {
                id: 2,
                opened: false,
                parent: "ATT",
                account: "ATT - KY",
                location: "2901 Dixie Hwy"
            },
            {
                id: 3,
                opened: false,
                parent: "CHARTER",
                account: "CHARTER- OH",
                location: "Brownsboro Road"
            },
            {
                id: 4,
                opened: false,
                parent: "KROGER",
                account: "KROGER - KY",
                location: "123 Happy Street"
            },
            {
                id: 5,
                opened: false,
                parent: "MARATHON",
                account: "MARATHON ASHLAND",
                location: "11631 US Route 23"
            }
        ],
        breadcrumbs: [
            {
                id: 1,
                title: "Accounts"
            }
        ]
    } 
    
    render() {
        return (            
            <Fragment>
                <div className="side-nav slider">
                    <BreadCrumbs /> {/* TO-DO */}
                    <div className="side-nav-wrap">
                        <ul className="nav flex-column">
                            <li className="nav-item"><NavLink className="nav-link side-nav-link" to="/accounts" activeClassName="active">ACCOUNTS</NavLink></li>
                            <li className="nav-item"><NavLink className="nav-link side-nav-link" to="/locations" activeClassName="active">LOCATIONS</NavLink></li>
                            <li className="nav-item"><NavLink className="nav-link side-nav-link" to="/work-orders" activeClassName="active">WORK ORDERS</NavLink></li>
                            <li className="nav-item"><NavLink className="nav-link side-nav-link" to="/parts" activeClassName="active">PARTS</NavLink></li>
                            <li className="nav-item"><NavLink className="nav-link side-nav-link" to="/follow-ups" activeClassName="active">FOLLOW UPS</NavLink></li>
                            <li className="nav-item"><NavLink className="nav-link side-nav-link side-nav-dim" to="/profile" activeClassName="active"><i className="fas fa-cog"> </i> SETTINGS</NavLink></li>
                            <li className="nav-item"><NavLink className="nav-link side-nav-link side-nav-dim" to="/support" activeClassName="active"><i className="fas fa-question-circle"> </i> HELP</NavLink></li>
                        </ul>                        
                    </div>
                    <DatePicker />{/* TO-DO */}
                    <div className="side-container">
                        { this.state.sidecontent.map((side) => (
                            <AccordionSection 
                                key={side.id}
                                side={side}
                            />       
                        ))}
                    </div> 
                </div>
            </Fragment>
        )
    }
}

export default  SideShow;
