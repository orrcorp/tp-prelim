import React, { Component, Fragment } from 'react';

class AccordionSection extends Component {
    state = {
        opened: this.props.side.opened,
        parent: this.props.side.parent,
        account: this.props.side.account,
        location: this.props.side.location
    }
    toggled = () => {
        this.setState({ opened: !this.state.opened, }) 
        console.log("Side Accordion") }
    
    render() {
        const { id, account, location, parent } = this.props.side;
        return (
            <Fragment>                 
                 <button key={ id } className="side-accordion" onClick={ this.toggled }>{parent}<div><i className="arrow "></i></div></button> 
                 <ul className="nav flex-column">
                    { this.state.opened && (
                        <Fragment>
                            <div className="side-accordion-content"><a href="/" className="nav-link"> <p>{ account }: <br></br>{ location }</p> </a></div>
                        </Fragment>
                        )}
                </ul>
            </Fragment>
        );
    }
}

export default AccordionSection;
