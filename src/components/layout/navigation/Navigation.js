import React, { Component, Fragment } from 'react';
import { Link, NavLink } from 'react-router-dom';
import '../../../sass/Navigation.scss';

class Navigation extends Component {
    constructor(props) {
        super(props);
        this.state = { menu: false };
        this.toggleMenu = this.toggleMenu.bind(this);
    }
    
    toggleMenu(){
        this.setState({ menu: !this.state.menu })
    }

    // TODO - ES6 ternary
    sideToggle = (e) => {
        e.preventDefault();
        let element = document.getElementsByClassName('slider');
        for (var i=0; i < element.length; i++) {
            element[i].classList.toggle('animate');
        }
        console.log("Menu Slider ");
    }

    render() {
        const show = (this.state.menu) ? "show" : "" ;
        return (
            <Fragment> 
                <nav className="navbar navbar-expand-lg fixed-top navbar-dark bg-dark flex-md-nowrap p-0 shadow slider">                    
                    <div className="navbar-brand">
                        <a href="/" onClick={ this.sideToggle }>
                            <svg width="30" height="30">
                                <path d="M0,5 30,5" stroke="#fff" strokeWidth="5" />
                                <path d="M0,14 30,14" stroke="#fff" strokeWidth="5" />
                                <path d="M0,23 30,23" stroke="#fff" strokeWidth="5" />
                            </svg>                            
                        </a>
                    </div>
                    <button className="navbar-toggler hide-menu" type="button" onClick={ this.toggleMenu } data-target="#navToggle">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className={`collapse navbar-collapse hide-menu ${show}`} id="navToggle">
                        <ul className="navbar-nav ml-5">
                            <li className="nav-item"><NavLink className="nav-link top-nav-link" to="/accounts" activeClassName="active">ACCOUNTS</NavLink></li>
                            <li className="nav-item"><NavLink className="nav-link top-nav-link" to="/locations" activeClassName="active">LOCATIONS</NavLink></li>
                            <li className="nav-item"><NavLink className="nav-link top-nav-link" to="/work-orders" activeClassName="active">WORK ORDERS</NavLink></li>
                            <li className="nav-item"><NavLink className="nav-link top-nav-link" to="/parts" activeClassName="active">PARTS</NavLink></li>
                            <li className="nav-item"><NavLink className="nav-link top-nav-link" to="/follow-ups" activeClassName="active">FOLLOW UPS</NavLink></li>
                            <span className="user-menu user-settings"> <Link to="/profile"><i className="fas fa-cog"></i></Link></span>
                            <span className="user-menu user-help"> <Link to="/support"><i className="fas fa-question-circle"></i></Link> </span>
                        </ul>
                    </div>
                </nav>
            </Fragment>
        )
    }
}

export default Navigation;
