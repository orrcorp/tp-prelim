import React, { Component, Fragment } from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import { AccountCardContainers } from '../layout/mainstage/cards/CardContainers';
import Navigation from '../layout/navigation/Navigation';
import SideShow from '../layout/sideshow/SideShow';
import LoadingSpinner from '../elements/LoadingSpinner';

const ACCOUNTS_QUERY = gql`
    query AccountsQuery {
        accounts {
            id
            name
            account_id__c
          }
    }`;

export class Accounts extends Component {

    render() {
        return (             
            <Fragment>
                <Navigation />
                <SideShow /> 
                <div className="main slider">                    
                    <Query query={ACCOUNTS_QUERY}>  
                            {({ loading, error, data }) => {
                                if(loading) return <div className="container"><LoadingSpinner /></div>
                                if(error) console.log(error);
                                return (
                                    <div className="container-fluid card-back">                                    
                                        {data.accounts.map( account => (
                                            <div className="col-md-12" >
                                                <AccountCardContainers 
                                                    key = { account.id } 
                                                    col_1 ={ account.name }
                                                    col_2 ={ account.account_id__c }
                                                    col_3 ={ account.id }
                                                />
                                            </div>
                                        ))}                                    
                                    </div>
                                )       
                            }}
                    </Query>
                </div>
            </Fragment>
        )
    }
}

export default Accounts;
