import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import logo from '../../images/logo.png';

export class Login extends Component {
    componentWillMount(){ document.body.classList.toggle('dark-gradient') }
    componentWillUnmount(){ document.body.classList.toggle('dark-gradient') }

    render() {
        return (
            <Fragment>
                <div className="container login-container" style={{minHeight: "220px;"}}>
                    <div className="row login-logo">
                        <div className="col-md-12 text-center">
                            <img src={logo} alt=""/>
                        </div>                        
                    </div>
                    <div className="row">
                        <div className="col-md-6 offset-md-3">
                            <form action="" className="justify-content-center">
                                <div className="form-group row">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fas fa-user"></i></div>
                                        </div>
                                        <input type="text" class="form-control" id="user" placeholder="username" />
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fas fa-lock"></i></div>
                                        </div>
                                        <input type="password" class="form-control" id="password" placeholder="password" />
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <div className="input-group">
                                        <Link class="btn btn-orr btn-lg btn-block" to="/home">Login</Link>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <div className="input-group">
                                        <Link className="login-help-text" to="/support"><p>Need Help With Your Login?</p></Link>
                                    </div>
                                </div>                                
                            </form>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default Login;
