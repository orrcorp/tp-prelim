import React, { Component, Fragment } from 'react';
import Navigation from '../layout/navigation/Navigation';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import LoadingSpinner from '../elements/LoadingSpinner';
import SideShow from '../layout/sideshow/SideShow'
import { SFCardRow, NRCardRow } from '../layout/mainstage/cards/CardRows';

// const ACCOUNTS_QUERY = gql`
//     query AccountsQuery {
//         accounts {
//             id
//             name
//             account_id__c
//           }
//     }`;

const WORK_ORDERS_ALL = gql`
query WorkOrdersAll {
    workOrders {
        id
        name
        svmxc__group_member__c
        svmxc__company__c
        location_lookup__c
        svmxc__street__c
        svmxc__state__c
        svmxc__city__c
        svmxc__zip__c
        svmxc__contact__c
        contact_phone__c
    }
}`;

const SITE_COMPONENTS_QUERY = gql`
query SiteComponents {
	siteComponents {
    barCode
    locationId
    dateAsseted
  }
}`;

export class TestPage extends Component {
    
    render() {
        return (            
            <Fragment>
                <Navigation />
                <SideShow />
                <div className="container main">
                    <div className="row">
                        <div className="col">
                            <div className="jumbotron" style={{ padding: "10px", paddingLeft: "30px" }}>
                                <h3 className="display-4" style={{color: "#333"}}>Apollo Server Test Connection</h3>                            
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <div className="jumbotron">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="row"><h2>Salesforce Output</h2></div><hr/>
                                            {/* <Query query={ACCOUNTS_QUERY}>  
                                                {({ loading, error, data }) => {
                                                    if(loading) return <div className="container"><LoadingSpinner /></div>
                                                    if(error) console.log(error);
                                                    return (
                                                        <Fragment>
                                                            {data.accounts.map( account => (
                                                                <div className="col-md-12" style={{marginBottom: "15px"}}>
                                                                    <SFCardRow 
                                                                        key = { account.id } 
                                                                        col_1 ={ account.name }
                                                                        col_2 ={ account.account_id__c }
                                                                        col_3 ={ account.id }
                                                                    />
                                                                </div>
                                                            ))}
                                                        </Fragment>
                                                    )       
                                                }}
                                            </Query>  */}
                                            <Query query={WORK_ORDERS_ALL}>  
                                                {({ loading, error, data }) => {
                                                    if(loading) return <div className="container"><LoadingSpinner /></div>
                                                    if(error) console.log(error);
                                                    return (
                                                        <Fragment>
                                                            {data.workOrders.map( workOrder => (
                                                                <div className="col-md-12" style={{marginBottom: "15px"}}>
                                                                    <SFCardRow 
                                                                        key = { workOrder.id } 
                                                                        col_1 ={ workOrder.id }
                                                                        col_2 ={ workOrder.name }
                                                                        col_3 ={ workOrder.svmxc__group_member__c }
                                                                        col_4 ={ workOrder.svmxc__company__c }
                                                                        col_5 ={ workOrder.location_lookup__c }
                                                                        col_6 ={ workOrder.svmxc__street__c }
                                                                        col_7 ={ workOrder.svmxc__state__c }
                                                                        col_8 ={ workOrder.svmxc__city__c }
                                                                        col_9 ={ workOrder.svmxc__zip__c }
                                                                        col_10 ={ workOrder.svmxc__contact__c }
                                                                        col_11 ={ workOrder.contact_phone__c }
                                                                    />
                                                                </div>                                                                        
                                                            ))}
                                                        </Fragment>
                                                    )       
                                                }}
                                            </Query>    
                                            </div>
                                            <div className="col-md-6">
                                                <div className="row"><h2>NetReport Output</h2></div><hr/>
                                                <Query query={SITE_COMPONENTS_QUERY}>  
                                                    {({ loading, error, data }) => {
                                                        if(loading) return <div className="container"><LoadingSpinner /></div>
                                                        if(error) console.log(error);
                                                            return (
                                                            <Fragment>
                                                                {data.siteComponents.map( siteComponent => (
                                                                    <div className="col-md-12" style={{marginBottom: "15px"}}>
                                                                        <NRCardRow 
                                                                            key = { siteComponent.barCode } 
                                                                            col_1 ={ siteComponent.locationId }
                                                                            col_2 ={ siteComponent.dateAsseted }
                                                                            col_3 ={ siteComponent.barCode }
                                                                        />
                                                                    </div>
                                                                ))}
                                                            </Fragment>
                                                        )       
                                                    }}
                                                </Query> 
                                            </div>
                                    </div>
                                </div>
                            </div>                      
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default TestPage;

