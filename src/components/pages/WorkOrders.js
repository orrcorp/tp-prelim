import React, { Component, Fragment } from 'react';
import Navigation from '../layout/navigation/Navigation';
import SideShow from '../layout/sideshow/SideShow';
import { WorkOrderCardContainers } from '../layout/mainstage/cards/CardContainers';

class WorkOrders extends Component {
    state = {
        wo_siderail: [
            {
                id: 1,
                opened: false,
                parent: "AMGEN",
                account: "AMGEN - KY",
                location: "3223 Shelbyville Rd"        
            },
            {
                id: 2,
                opened: false,
                parent: "ATT",
                account: "ATT - KY",
                location: "2901 Dixie Hwy"
            },
            {
                id: 3,
                opened: false,
                parent: "CHARTER",
                account: "CHARTER- OH",
                location: "Brownsboro Road"
            }
        ],
        wo_main: [
            {
                id: 11,
                wo_opened: false,
                wo_number: "00239301",
                wo_location: "1200 Plantside Drive",
                wo_city: "Louisville",
                wo_state: " KY",
                wo_zip: "  40031",
                wo_inst_prod: "FENWAL FN6000 FIRE ALARM AND HALON IN SUBFLOOR",
                wo_event_date: "03/16/19",
                wo_event_time: "12:00pm",
                wo_multi_tech: "",
                wo_multi_day: "checked",
                wo_parent: "CERIDIAN",
                wo_account: "CERIDIAN - KY",
                wo_address: "10301 LINN STATION ROAD",
                wo_site_contact: "DAVE HENNIGAR",
                wo_site_contact_phone: "204-975-8825",
                wo_product: "Fenwal Halon 1301 Fire Suppression",
                wo_order_type: "PM",
                wo_visit_type: "Maintenance",
                wo_po_number: "30465",
                wo_cust_work_ticket: "WT587441",
                wo_access_hours: "9am - 5pm",
                wo_previous_notes: "Wipe your feet upon entering building.",
                wo_location_notes: "Elevators run on tokens.",
                asset_component_class: "Control Panel Circuits",
                asset_component_type: "Signaling Line Circuit",
                asset_manufacturer: "Unknown",
                asset_model: "Unknown Conventional",
                asset_onsite: "1",
                asset_due: "1",
                asset_plus_12: "0"
            },
            {
                id: 12,
                wo_opened: false,
                wo_number: "00239302",
                wo_location: "123 Pinoak Dr",
                wo_city: "La Porte",
                wo_state: " TX",
                wo_zip: "  77571",
                wo_inst_prod: "Xtralis Air Sampler",
                wo_event_date: "03/15/19",
                wo_event_time: "4:00pm",
                wo_multi_tech: "checked",
                wo_multi_day: "",
                wo_parent: "AMGEN",
                wo_account: "AMGEN - TX",
                wo_address: "123 Main Street",
                wo_site_contact: "Joe Schmo",
                wo_site_contact_phone: "502-222-2202",
                wo_product: "Air Sampler",
                wo_order_type: "PM",
                wo_visit_type: "Repair",
                wo_po_number: "21236",
                wo_cust_work_ticket: "WT35564",
                wo_access_hours: "9am - 5pm",
                wo_previous_notes: "Beware of sleeping security guards.",
                wo_location_notes: "Free breakfast for visiting techs",
                asset_component_class: "Extinguishing Equipment",
                asset_component_type: "Halon Cylinder Top Discharge",
                asset_manufacturer: "Unknown",
                asset_model: "Unknown Conventional",
                asset_onsite: "2",
                asset_due: "1",
                asset_plus_12: "0"
            },
            {
                id: 13,
                wo_opened: false,
                wo_number: "00239303",
                wo_location: "5005 FLEMINGSBURG RD",
                wo_city: "MOREHEAD",
                wo_state: " KY",
                wo_zip: "  40351",
                wo_inst_prod: "Fire Suppression",
                wo_event_date: "03/18/19",
                wo_event_time: "9:00am",
                wo_multi_tech: "checked",
                wo_multi_day: "checked",
                wo_parent: "CHARTER",
                wo_account: "CHARTER LEGACY TWC & BRIGHTHOUSE - KY",
                wo_address: "5005 FLEMINGSBURG RD",
                wo_site_contact: "Front Desk",
                wo_site_contact_phone: "502-222-2202",
                wo_product: "ECARO-25 Sphere",
                wo_order_type: "PM",
                wo_visit_type: "Repair",
                wo_po_number: "165517",
                wo_cust_work_ticket: "WT35774",
                wo_access_hours: "9am - 5pm",
                wo_previous_notes: "Don't eat 30 minutes before swimming.",
                wo_location_notes: "Vending machines are monitored for excessive use.",
                asset_component_class: "Extinguishing Equipment",
                asset_component_type: "Halon Cylinder Top Discharge",
                asset_manufacturer: "Unknown",
                asset_model: "Unknown Conventional",
                asset_onsite: "13",
                asset_due: "10",
                asset_plus_12: "2"
            }
        ]
    }
    render() {
        return (
            <Fragment>
                <Navigation />
                <SideShow />
                <div className="main slider">
                    <div className="container-fluid card-back">
                        {this.state.wo_main.map((wos) => (
                            <WorkOrderCardContainers 
                                key ={ wos.id }
                                opened={ wos.wo_opened }
                                col_1 ={ wos.wo_number }
                                col_2 ={ wos.wo_location }
                                col_3 ={ wos.wo_city }
                                col_4 ={ wos.wo_state }
                                col_5 ={ wos.wo_zip }
                                col_6 ={ wos.wo_inst_prod }
                                col_7 ={ wos.wo_event_date }
                                col_8 ={ wos.wo_event_time }
                                col_9 ={ wos.wo_multi_tech }
                                col_10 ={ wos.wo_multi_day }
                                col_11 ={ wos.wo_parent }
                                col_12 ={ wos.wo_account }
                                col_13 ={ wos.wo_address }
                                col_14 ={ wos.wo_site_contact }
                                col_15 ={ wos.wo_site_contact_phone }
                                col_16 ={ wos.wo_product }
                                col_17 ={ wos.wo_order_type }
                                col_18 ={ wos.wo_visit_type }
                                col_19 ={ wos.wo_po_number }
                                col_20 ={ wos.wo_cust_work_ticket }
                                col_21 ={ wos.wo_access_hours }
                                col_22 ={ wos.wo_previous_notes }
                                col_23 ={ wos.wo_location_notes }
                                col_24 ={ wos.asset_component_class }
                                col_25 ={ wos.asset_component_type }
                                col_26 ={ wos.asset_manufacturer }
                                col_27 ={ wos.asset_model }
                                col_28 ={ wos.asset_onsite }
                                col_29 ={ wos.asset_due }
                                col_30 ={ wos.asset_plus_12 }
                            />
                        ))}
                    </div>    
                </div>
            </Fragment>
        )
    }
}

export default WorkOrders;
