import React, { Component, Fragment } from 'react';
import Navigation from '../layout/navigation/Navigation';
import SideShow from '../layout/sideshow/SideShow';
import { PartsCardContainers } from '../layout/mainstage/cards/CardContainers';

class Parts extends Component {
    state = {
        part_siderail: [
            {
                id: 1,
                opened: false,
                parent: "AMGEN",
                account: "AMGEN - KY",
                location: "3223 Shelbyville Rd"        
            },
            {
                id: 2,
                opened: false,
                parent: "ATT",
                account: "ATT - KY",
                location: "2901 Dixie Hwy"
            },
            {
                id: 3,
                opened: false,
                parent: "CHARTER",
                account: "CHARTER- OH",
                location: "Brownsboro Road"
            }
        ],
        part_main: [
            {
                id: 11,
                parts_opened: false,      
                part_wo_num: "WO-00113378",
                part_line_num: "000004441",
                part_part_num: "53321487",
                part_descr: "BAT12-12",
                part_ord_amt: "10",
                part_ord_rec: "08",
                part_ord_status: "Received",
                part_parent: "ATT",
                part_acct: "ATT - FL",
                part_loc: "FL1749 - 2901 Dixie Hwy, Oakland Park ",
                part_ord_num: "53321487",
                part_order_date: "03/12/19",
                part_exp_ship: "03/13/19",
                part_exp_rec: "03/18/19",
                part_act_rec: "03/19/19",
                part_ship_type: "Priority",
                part_status: "In Transit",
                part_courier: "FEDEX",
                part_priority: "2 Day Air",
                part_track: "ZG3897898646852826",
                part_sage_po: "123456789",
                part_from_loc: "BREA - SO CAL - 550",
                part_to_loc: "RENE SUAREZ - 552",
                part_notes: "Backorder replacement"
            },
            {
                id: 12,
                parts_opened: false,
                part_wo_num: "WO-00113334",
                part_line_num: "000003439",
                part_part_num: "555434555",
                part_descr: "Fire Extinguisher Hanger EXT Model",
                part_ord_amt: "3",
                part_ord_rec: "3",
                part_ord_status: "Received",
                part_parent: "AMGEN",
                part_acct: "AMGEN - KY",
                part_loc: "3223 Shelbyville Rd",
                part_ord_num: "555434555",
                part_order_date: "03/19/19",
                part_exp_ship: "03/20/19",
                part_exp_rec: "03/23/19",
                part_act_rec: "03/23/19",
                part_ship_type: "Priority",
                part_status: "Delivered",
                part_courier: "UPS",
                part_priority: "Ground",
                part_track: "G4389778606852826",
                part_sage_po: "9876543210",
                part_from_loc: "Atlanta",
                part_to_loc: "Louisville",
                part_notes: "Order #HJ67987 missing components"
            },
            {
                id: 13,
                parts_opened: false,
                part_wo_num: "WO-00113334",
                part_line_num: "000003822",
                part_part_num: "77854125",
                part_descr: "Halon System",
                part_ord_amt: "9",
                part_ord_rec: "9",
                part_ord_status: "Received",
                part_parent: "SPECTRUM",
                part_acct: "SPECTRUM - GA",
                part_loc: "Atlanta Tower 17",
                part_ord_num: "77854125",
                part_order_date: "03/19/19",
                part_exp_ship: "03/20/19",
                part_exp_rec: "03/23/19",
                part_act_rec: "03/23/19",
                part_ship_type: "Priority",
                part_status: "Delivered",
                part_courier: "USPS",
                part_priority: "Pony",
                part_track: "G4389778606852826",
                part_sage_po: "9876543210",
                part_from_loc: "Atlanta",
                part_to_loc: "Miami",
                part_notes: "(1) 87-120042-001 SVA, (1) 87-120043-001 System Cartridge, (1) 87-120044-001 Test Cartridge, (1) 87-120058-001 EMT Connector Kit, (1) 87-120039-001 SPDT Microswitch Kit, (1) 87-120039-501 SPDT Microswitch Kit"
            }
        ]   
    }
    
    render() {
        return (
            <Fragment>
                <Navigation />                
                <SideShow />
                <div className="main slider">               
                    <div className="container-fluid card-back">
                        { this.state.part_main.map((part) => (
                            <PartsCardContainers
                                key ={part.id}
                                opened = {part.parts_opened}
                                col_1 = { part.part_wo_num }
                                col_2 = { part.part_line_num }
                                col_3 = { part.part_part_num }
                                col_4 = { part.part_descr }
                                col_5 = { part.part_ord_amt }
                                col_6 = { part.part_ord_rec }
                                col_7 = { part.part_ord_status }
                                col_8 = { part.part_parent }
                                col_9 = { part.part_acct }
                                col_10 = { part.part_loc }
                                col_11 = { part.part_ord_num }
                                col_12 = { part.part_order_date }
                                col_13 = { part.part_exp_ship }
                                col_14 = { part.part_exp_rec }
                                col_15 = { part.part_act_rec }
                                col_16 = { part.part_ship_type }
                                col_17 = { part.part_status }
                                col_18 = { part.part_courier }
                                col_19 = { part.part_priority }
                                col_20 = { part.part_track }
                                col_21 = { part.part_sage_po }
                                col_22 = { part.part_from_loc }
                                col_23 = { part.part_to_loc }
                                col_24 = { part.part_notes }
                            />
                        ))}
                    </div>    
                </div> 
            </Fragment>
        )
    }
}

export default Parts;
