import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { SupportCardContainers } from '../layout/mainstage/cards/CardContainers';
import Navigation from '../layout/navigation/Navigation';
import '../../sass/Support.scss'

class Support extends Component {
    state = {
        support: [
            {
                id: 1,
                opened: false,
                support_title: " Does Chuck Norris have the greatest 'Poker-Face' of all time?",
                support_answer: "He won the 1983 World Series of Poker, despite holding only a Joker, a Get out of Jail Free Monopoloy card, a 2 of clubs, 7 of spades and a green #4 card from the game UNO.",
                support_link_title: "Click Here"        
            },
            {
                id: 2,
                opened: false,
                support_title: "What is the revenue goal for Taco Bell in 2020?",
                support_answer: "With 9000 restaurants and 46 million customers, Taco Bell expects $15 billion in sales for 2020.",
                support_link_title: "Click Here" 
            },
            {
                id: 3,
                opened: false,
                support_title: "What is the definition of Cenosillicaphobia?",
                support_answer: "Cenosillicaphobia is the fear of an empty beer glass.",
                support_link_title: "Click Here" 
            },
            {
                id: 4,
                opened: false,
                support_title: "How far away is the Andromeda Galaxy?",
                support_answer: "The Andromeda Galaxy (M31) is the closest spiral galaxy to us, and though it's gravitationally bound to the Milky Way, it's not the closest galaxy by far – being 2 million light years away",
                support_link_title: "Click Here" 
            },
            {
                id: 5,
                opened: false,
                support_title: "Who is the strongest Green Lantern?",
                support_answer: "Hal Jordan is the most powerful Green Lantern to ever wield the ring?",
                support_link_title: "Click Here" 
            },
            {
                id: 6,
                opened: false,
                support_title: "How fast is a Lamborghini Aventador?",
                support_answer: "With it's 740hp V-12, the Aventador can reach 0-60 in 2.9 seconds and has a top speed of 219mph.",
                support_link_title: "Click Here" 
            }
        ]
    }

    render() {
        return (
            <Fragment >
                <Navigation /> 
                <div className="main slider" style={{marginLeft: '0px'}}>                    
                    <div className="support-wrapper">  {/*  Search Button Container */}
                        <div className="row">                     
                            <div className="col-lg-12">
                                <h2 className="display-4 support-header">Help and Support</h2>
                                <div className="input-group">
                                    <input type="text" className="form-control support-search" placeholder="Start your search here ..." aria-label="Search" aria-describedby="search" />
                                    <div className="input-group-append">
                                        <button className="btn btn-success support-search-button" type="button" id="search-button">SEARCH</button>
                                    </div>
                                </div>         
                            </div>
                        </div>
                    </div>
                    <div className="container-fluid card-back">
                        {this.state.support.map((sup) => (
                            <SupportCardContainers 
                                key = {sup.id}
                                opened = {sup.opened}
                                col_1 = {sup.support_title}
                                col_2 = {sup.support_answer}
                                col_3 = {sup.support_link_title}
                            />
                        ))}
                    </div>
                    <div className="row" style={{marginTop: '30px'}}>
                        <div className="col-md-3">
                            <Link class="btn btn-success btn-lg btn-block" to="/home">Home</Link>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default Support;
