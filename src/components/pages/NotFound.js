import React, { Component, Fragment } from 'react';
import Navigation from '../layout/navigation/Navigation';
import SideShow from '../layout/sideshow/SideShow';
import LoadingSpinner from '../elements/LoadingSpinner';

export class NotFound extends Component {
    render() {
        return (
            <Fragment >
                <Navigation />
                <SideShow />
                <div className="main slider">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="test-span">
                                    <h3>404 Not Found</h3>
                                    <p style={{color: '#fff'}}>(Coming Soon, in the meantime, enjoy a spinner)</p>
                                    <LoadingSpinner />
                                </div>
                            </div>
                        </div>
                    </div>     
                </div>
            </Fragment>
        )
    }
}

export default NotFound;
