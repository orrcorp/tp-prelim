import React, { Component, Fragment } from 'react';
import { DashCardContainers } from '../layout/mainstage/cards/CardContainers';

class Home extends Component {
    state = {
        dash_cards: [
            {
                id: '1',
                dash_card_header: 'Total Parts Orders',
                dash_card_large: false,
                dash_card_col: '4',
                dash_card_total: '555',
                dash_card_link: 'parts',
                dash_card_buttonTitle: 'View Parts Orders'
            },
            {
                id: '2',
                dash_card_header: 'Open Parts Orders',
                dash_card_large: false,
                dash_card_col: '4',
                dash_card_total: '222',
                dash_card_link: 'parts',
                dash_card_buttonTitle: 'View Parts Orders'
            },
            {
                id: '3',
                dash_card_header: 'Total Components',
                dash_card_large: false,
                dash_card_col: '4',
                dash_card_total: '777',
                dash_card_link: 'accounts',
                dash_card_buttonTitle: 'View Summary'
            },
            {
                id: '4',
                dash_card_header: 'Per Day Visit Count',
                dash_card_large: false,
                dash_card_col: '4',
                dash_card_total: '333',
                dash_card_link: 'work-orders',
                dash_card_buttonTitle: 'View Work Orders'
            },
            {
                id: '5',
                dash_card_header: 'Total Locations',
                dash_card_large: false,
                dash_card_col: '4',
                dash_card_total: '888',
                dash_card_link: 'locations',
                dash_card_buttonTitle: 'View Locations'
            },
            {
                id: '6',
                dash_card_header: 'Total Work Orders',
                dash_card_large: false,
                dash_card_col: '4',
                dash_card_total: '999',
                dash_card_link: 'work-orders',
                dash_card_buttonTitle: 'View Work Orders'
            },
            {
                id: '7',
                dash_card_header: 'Approved Follow Ups',
                dash_card_large: false,
                dash_card_col: '4',
                dash_card_total: '999',
                dash_card_link: 'follow-ups',
                dash_card_buttonTitle: 'View Follow Ups'
            },
            {
                id: '8',
                dash_card_header: 'Office Contact Info',
                dash_card_large: true, // if true, display dash contact component, otherwise display  reg small one
                dash_card_col: '8',
                dash_card_total: '333',
                dash_card_link: 'temp',
                dash_card_buttonTitle: 'Temp Page'
            }
        ]
    }

    render() {
        return (
            <Fragment>
                <div className="container dash-container mx-auto">
                    {/* TO-DO - Welcome Head Panel: Name, Tech ID, Date */}
                    <div className="row"><div className="card-header"><h1 className="display-3 mx-4 mb-4"> Dispatch Overview</h1></div></div>                    
                    <div className="row">                    
                        {this.state.dash_cards.map((dash_crds) => (
                            <DashCardContainers
                                key = { dash_crds.id }                            
                                field1 = { dash_crds.dash_card_header }
                                field2 = { dash_crds.dash_card_large }
                                field3 = { dash_crds.dash_card_col }
                                field4 = { dash_crds.dash_card_total }
                                field5 = { dash_crds.dash_card_link }
                                field6 = { dash_crds.dash_card_buttonTitle }
                            />
                        ))}
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default Home;
