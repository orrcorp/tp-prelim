import React, { Component, Fragment } from 'react';
import Navigation from '../layout/navigation/Navigation';
import SideShow from '../layout/sideshow/SideShow';
import LoadingSpinner from '../elements/LoadingSpinner';

class Profile extends Component {
    render() {
        return (
            <Fragment >
                <Navigation />
                <SideShow />
                <div className="main slider">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="test-span">
                                    <h3>Profile Settings</h3>
                                    <p style={{color: '#fff'}}>(Coming Soon, Just a loader for your viewing pleasure)</p>
                                    <LoadingSpinner />
                                </div>
                            </div>
                        </div>
                    </div>     
                </div>
            </Fragment>
        )
    }
}

export default Profile;
