import React, { Component, Fragment } from 'react';
import Navigation from '../layout/navigation/Navigation';
import SideShow from '../layout/sideshow/SideShow';
import { LocationCardContainers } from '../layout/mainstage/cards/CardContainers';

class Locations extends Component {
    state = {
        loc_siderail: [
            {
                id: 1,
                opened: false,
                parent: "AMGEN",
                account: "AMGEN - KY",
                location: "3223 Shelbyville Rd"        
            },
            {
                id: 2,
                opened: false,
                parent: "ATT",
                account: "ATT - KY",
                location: "2901 Dixie Hwy"
            },
            {
                id: 3,
                opened: false,
                parent: "CHARTER",
                account: "CHARTER- OH",
                location: "Brownsboro Road"
            }
        ],
        loc_main: [
            {
                id: 11,
                loc_class: "Halon System",
                loc_part: "Fenwall Halon 1301 Fire Supp",
                loc_manufacturer: "Fenwall",
                loc_model: "Fenwall Halon 1301 Fire Supp",
                loc_onsite: "05",
                loc_due: "03"
            },
            {
                id: 12,
                loc_class: "Fire Suppression",
                loc_part: "Fenwall 2320 FM200",
                loc_manufacturer: "Fenwall",
                loc_model: "Fenwall 2320 FM200",
                loc_onsite: "20",
                loc_due: "10"
            },
            {
                id: 13,
                loc_class: "Battery",
                loc_part: "BAT12-12",
                loc_manufacturer: "Duracell",
                loc_model: "BAT12-12",
                loc_onsite: "10",
                loc_due: "08"
            }
        ],
        loc_header: [
            {
                id: 1,
                loc_head_field1: "CLASS",
                loc_head_field2: "PART",
                loc_head_field3: "MFR",
                loc_head_field4: "MODEL",
                loc_head_field5: "ON SITE",
                loc_head_field6: "DUE"
            }
        ]
    }

    render() {
        return (
            <Fragment>
                <Navigation />                
                <SideShow />
                <div className="main slider">               
                    <div className="container-fluid card-back">
                        { this.state.loc_main.map((loc) => (
                            <LocationCardContainers
                                key ={loc.id}
                                col_1 ={ loc.loc_class }
                                col_2 ={ loc.loc_part }
                                col_3 ={ loc.loc_manufacturer }
                                col_4 ={ loc.loc_model }
                                col_5 ={ loc.loc_onsite }
                                col_6 ={ loc.loc_due }
                            />
                        ))}
                    </div>    
                </div> 
            </Fragment>
        )
    }
}

export default Locations;
