import React, { Component, Fragment } from 'react';
import Navigation from '../layout/navigation/Navigation';
import SideShow from '../layout/sideshow/SideShow';
import {FollowUpCardContainers} from '../layout/mainstage/cards/CardContainers';

class FollowUps extends Component {
    state = {
        f_up_siderail: [
            {
                id: 1,
                opened: false,
                parent: "AMGEN",
                account: "AMGEN - KY",
                location: "3223 Shelbyville Rd"        
            },
            {
                id: 2,
                opened: false,
                parent: "ATT",
                account: "ATT - KY",
                location: "2901 Dixie Hwy"
            },
            {
                id: 3,
                opened: false,
                parent: "CHARTER",
                account: "CHARTER- OH",
                location: "Brownsboro Road"
            }
        ],
        f_up_main: [
            {
                id: 11,
                f_up_wo_number: "00239305",
                f_up_location: "KROGER - OH",
                f_up_inst_prod: "Suppression System",
                f_up_barcode: "98761234",
                f_up_condition: "New",
                f_up_action: "Install",
                f_up_status: "Pre-Approved",
                f_up_other_notes: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Reiciendis, qui. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Reiciendis, qui"
            },          
            {
                id: 12,
                f_up_wo_number: "00239306",
                f_up_location: "ATT - FL",
                f_up_inst_prod: "Exit Lights and Signs",
                f_up_barcode: "55321445",
                f_up_condition: "New",
                f_up_action: "Replacement",
                f_up_status: "Approved",
                f_up_other_notes: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Reiciendis, qui. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Reiciendis, qui"
            },
            {
                id: 13,
                f_up_wo_number: "00239307",
                f_up_location: "SPECTRUM - KY",
                f_up_inst_prod: "Fire Extinguishers",
                f_up_barcode: "552269987",
                f_up_condition: "Current",
                f_up_action: "Service",
                f_up_status: "Pre-Approved",
                f_up_other_notes: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Reiciendis, qui. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Reiciendis, qui"
            }
        ],
        f_up_header: [
            {
                id: 1,
                f_up_head_field1: "WO#",
                f_up_head_field2: "LOCATION",
                f_up_head_field3: "PRODUCT",
                f_up_head_field4: "BCODE",
                f_up_head_field5: "COND",
                f_up_head_field6: "ACTION",
                f_up_head_field7: "STATUS",
                f_up_head_field8: "NOTES"
            }
        ]
    }

    render() {
        return (
            <Fragment>
                <Navigation />
                <SideShow />
                <div className="main slider">               
                    <div className="container-fluid card-back">
                        { this.state.f_up_main.map((f_up) => (
                            <FollowUpCardContainers
                                key ={f_up.id}
                                col_1 ={ f_up.f_up_wo_number }
                                col_2 ={ f_up.f_up_location }
                                col_3 ={ f_up.f_up_inst_prod }
                                col_4 ={ f_up.f_up_barcode }
                                col_5 ={ f_up.f_up_condition }
                                col_6 ={ f_up.f_up_action }
                                col_7 ={ f_up.f_up_status }
                                col_8 ={ f_up.f_up_other_notes }
                            />
                        ))}
                    </div>    
                </div> 
            </Fragment>
        )
    }
}

export default  FollowUps;
