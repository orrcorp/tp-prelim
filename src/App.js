import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
// Page Routes
import Login from './components/pages/Login';
import Home from './components/pages/Home';
import Accounts from './components/pages/Accounts';
import Locations from './components/pages/Locations';
import WorkOrders from './components/pages/WorkOrders';
import Parts from './components/pages/Parts';
import FollowUps from './components/pages/FollowUps';
import Profile from './components/pages/Profile';
import Support from './components/pages/Support';
import NotFound from './components/pages/NotFound';
import TempPage from './components/pages/TempPage';
// Styles
import './App.scss';

const client = new ApolloClient({
    uri: 'https://gqlaptest.orrprotection.com/graphql'
  });

// const client = new ApolloClient({
//     uri: 'http://localhost:4999/graphql'
//   });
  
class App extends Component {
    render() {
        return (
            <ApolloProvider client={client}>
                <Router>
                    <Switch>                        
                        <Route exact path="/" component={Login} />
                        <Route exact path="/login" component={Login} />
                        <Route exact path="/home" component={Home} />
                        <Route exact path="/accounts" component={Accounts} />
                        <Route exact path="/locations" component={Locations} />
                        <Route exact path="/work-orders" component={WorkOrders} />
                        <Route exact path="/parts" component={Parts} />
                        <Route exact path="/follow-ups" component={FollowUps} />
                        <Route exact path="/profile" component={Profile} />
                        <Route exact path="/support" component={Support} />
                        <Route exact path="/not-found" component={NotFound} />
                        <Route exact path="/temp" component={TempPage} />
                        </Switch>
                </Router>
            </ApolloProvider>
        );
    }
}

export default App;
